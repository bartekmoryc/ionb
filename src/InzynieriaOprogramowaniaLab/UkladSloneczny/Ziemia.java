package InzynieriaOprogramowaniaLab.UkladSloneczny;

/**
 * Created by bartlomiej.moryc on 11.02.2017.
 */
public class Ziemia {

    private static Ziemia instance = null;

    private Ziemia() {
        System.out.println("Jestem jedyna!");
    }

    public static Ziemia getInstance() {
        if (instance == null)
            instance = new Ziemia();
        System.out.println(instance);
        return instance;
    }
}
