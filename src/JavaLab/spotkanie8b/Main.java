package JavaLab.spotkanie8b;

/**
 * Created by bartlomiej.moryc on 29.01.2017.
 */
class Punkt {
    private int x;
    private int y;

    public Punkt(int x, int y) {
        this.x = x;
        this.y = y;
    }

    // kostruktor kopiujacy
    public Punkt(Punkt pkt){
        this.x = pkt.x;
        this.y = pkt.y;
    }

    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }
}


public class Main {
    public static void main(String[] args) {
        Punkt punkt1 = new Punkt(10, 10);
        Punkt punkt2 = punkt1;
        Punkt punkt3 = new Punkt(10, 10);
        Punkt punkt4 = new Punkt(punkt1.getX(), punkt1.getY());
        Punkt punkt5 = new Punkt(punkt1);

        boolean porownanie1 = punkt1==punkt2;
        boolean porownanie2 = punkt1==punkt3;

        System.out.println("Porownanie1: "+porownanie1);
        System.out.println("Porownanie2: "+porownanie2);
        wyswietl(punkt1, punkt2, punkt3, punkt4, punkt5);

        punkt2.setX(11);
        punkt3.setX(12);
        punkt4.setX(13);
        punkt5.setX(14);
        wyswietl(punkt1, punkt2, punkt3, punkt4, punkt5);
    }

    public static void wyswietl(Punkt... punkty) {
        int size = punkty.length;
        for(int i=0; i<size; i++) {
            System.out.println("Punkt "+ (i+1) +" x:"+punkty[i].getX()+" ; y:"+punkty[i].getY());
        }
        System.out.println();
    }
}