package JavaLab.spotkanie8b;


//import static org.junit.Assert.*;

/**
 * Created by bartlomiej.moryc on 29.01.2017.
 */
public class MyNumberTest {
    public static void main(String[] args) {

        MyNumber mn0 = new MyNumber(0);
        MyNumber mn1 = new MyNumber(3);
        MyNumber mn2 = new MyNumber(789);
        MyNumber mn3 = new MyNumber(-10);

        System.out.println(mn0);
        System.out.println(mn1);
        System.out.println(mn2);
        System.out.println(mn3);

        System.out.println(mn0.isOdd());
        System.out.println(mn1.isOdd());
        System.out.println(mn2.isOdd());
        System.out.println(mn3.isOdd());

        System.out.println(mn0.isEven());
        System.out.println(mn1.isEven());
        System.out.println(mn2.isEven());
        System.out.println(mn3.isEven());

        System.out.println(mn0.sqrt());
        System.out.println(mn1.sqrt());
        System.out.println(mn2.sqrt());
        System.out.println(mn3.sqrt());

        System.out.println(mn1.pow(mn0));
        System.out.println(mn2.pow(mn0));
        System.out.println(mn3.pow(mn0));

        System.out.println(mn1.pow(mn1));
        System.out.println(mn2.pow(mn1));
        System.out.println(mn3.pow(mn1));

        System.out.println(mn1.pow(mn2));
        System.out.println(mn2.pow(mn2));
        System.out.println(mn3.pow(mn2));

        System.out.println(mn1.pow(mn3));
        System.out.println(mn2.pow(mn3));
        System.out.println(mn3.pow(mn3));




    }


}