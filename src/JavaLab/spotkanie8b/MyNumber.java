package JavaLab.spotkanie8b;

/**
 * Created by bartlomiej.moryc on 29.01.2017.
 */
public class MyNumber {

    private double liczba;

    public MyNumber(double liczba){
        this.liczba = liczba;
    }

    public double getLiczba(){
        return this.liczba;
    }

    @Override
    public String toString(){
        return "" + liczba;
    }
    
    public boolean isOdd(){
        return this.liczba % 2 != 0;
    }

    public boolean isEven() { return this.liczba % 2 == 0; }

    public double sqrt(){
        return Math.sqrt(this.liczba);
    }

    public double pow(MyNumber x){
        return Math.pow(this.getLiczba(), x.getLiczba());
    }




}
