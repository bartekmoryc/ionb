package JavaLab.spotkanie8b;

/**
 * Created by bartlomiej.moryc on 29.01.2017.
 */
public class Human {
    private int age, weight, height;
    private String name, gender;

    public Human(int age, int weight, int height, String name, String gender){
        this.age = age;
        this.weight = weight;
        this.height = height;
        this.name = name;
        this.gender = gender;
        System.out.println("My name is " + name + ". I am a " + gender + ". I am " + age +" years old. I weigh " + weight + " kilograms and I'm " + height + " centimeters tall. I am Human!");
    }

    public int getAge(){
        return this.age;

    }

    public int getWeight(){
        return this.weight;

    }

    public int getHeight(){
        return this.height;

    }

    public String getName(){
        return this.name;
    }

    public boolean isMale(){
        return this.gender.equals("man");
    }





}
