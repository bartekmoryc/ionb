package JavaLab.spotkanie8b;

//import static org.junit.Assert.*;

/**
 * Created by bartlomiej.moryc on 29.01.2017.
 */
public class HumanTest {
    public static void main(String[] args) {

        Human h1 = new Human(34, 85, 184, "Bartek", "man");
        Human h2 = new Human(36, 56, 172, "Małgo", "woman");
        Human h3 = new Human(1, 10, 80, "Janka", "woman");

        System.out.println(h2.getAge());
        System.out.println(h1.getWeight());
        System.out.println(h3.getHeight());
        System.out.println(h3.getName());
        System.out.println(h2.isMale());



    }

}