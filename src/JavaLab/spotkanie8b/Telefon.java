package JavaLab.spotkanie8b;

/**
 * Created by bartlomiej.moryc on 12.02.2017.
 */
public class Telefon {

    // deklaracja pol
    private String numerTelefonu;
    private int lacznyCzasRozmow;
    private static double cenaRozmowy = 0.48;

    // konstruktor
    public Telefon (String numer) {
        numerTelefonu = numer;
    }

    // deklaracja metod
    public double obliczKwoteDoZaplaty() {
        return cenaRozmowy * (lacznyCzasRozmow / 60);
    }

    public static void ustawCeneRozmowy(double nowaCena){
        cenaRozmowy = nowaCena;
    }

    public void zadzwon(String nrTelefonu) {
        System.out.println ("Dzwoni4 do: " + nrTelefonu);
        System.out.println ("Dry5, dry5...");
        System.out.println ("Rozmowa w toku...");
        int czasRozmowy = (int) (Math.random()*3600);
        lacznyCzasRozmow += czasRozmowy;
        System.out.println ("Rozmowa zakonczona. ");
        System.out.printf ("Czas rozmowy: %d min. %d sek.", czasRozmowy/60, czasRozmowy%60);
    }
}

class Telefony {
    public static void main(String[] args){
        Telefon telefonAni = new Telefon("783982331");
        Telefon telefonJarka = new Telefon("608234982");
        telefonAni.zadzwon("0124239832");
        telefonJarka.zadzwon("112");
        double kwota = telefonAni.obliczKwoteDoZaplaty();
        System.out.printf("Ania ma do zaplaty %f zl.", kwota);
    }
}
