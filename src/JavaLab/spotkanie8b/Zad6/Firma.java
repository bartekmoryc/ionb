package JavaLab.spotkanie8b.Zad6;

/**
 * Created by bartlomiej.moryc on 12.02.2017.
 */
public class Firma {

    public static void main(String[] args) {

        Pracownik p1 = new Pracownik();
        Pracownik p2 = new Pracownik("Bartek", "Moryc");
        Pracownik p3 = new Pracownik(34);
        Pracownik p4 = new Pracownik("Henryk", "Kwiatek", 67);

        Object[] pracownicy = {p1, p2, p3, p4};

        for (Object p : pracownicy) {
            System.out.println(p.toString());
        }
        


    }

}
