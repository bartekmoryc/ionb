package JavaLab.spotkanie8b.Zad6;

/**
 * Created by bartlomiej.moryc on 12.02.2017.
 */
public class Pracownik {

    //pola
    private String name;
    private String surname;
    private int age;

    //konstruktory

    public Pracownik(){
        this.name = "John";
        this.surname = "Doe";
        this.age = 22;
    }

    public Pracownik(String name, String surname) {
        this.name = name;
        this.surname = surname;
        this.age = 22;
    }

    public Pracownik(int age) {
        this.name = "John";
        this.surname = "Doe";
        this.age = age;
    }

    public Pracownik(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Jestem " + name + " " + surname + " i mam " + age + " lata.";
    }
}
