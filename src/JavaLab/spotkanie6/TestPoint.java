package JavaLab.spotkanie6;



public class TestPoint {

    public static void main(String[] args) {

        // Create a point with with [0,0] coordinates
        System.out.println("Test konstruktora bezargumentowego");
        Point point1 = new Point();
        System.out.println("Koordynaty punktu to: " + point1.toString());
        System.out.println();

        // Create a circle with [3,7] coordinates
        System.out.println("Test konstruktora z argumentami (3,7)");
        Point point2 = new Point(3,7);
        System.out.println("Koordynaty punktu to: " + point2.toString());
        System.out.println();


        // Create a circle with [115,242] coordinates
        System.out.println("Test konstruktora z argumentami (115,242)");
        Point point3 = new Point(115,242);
        System.out.println("Koordynaty punktu to: " + point3.toString());
        System.out.println();

        // Create a circle with [-1234,90000] coordinates
        System.out.println("Test konstruktora z argumentami (-1234,90000)");
        Point point4 = new Point(-1234,90000);
        System.out.println("Koordynaty punktu to: " + point3.toString());
        System.out.println();

        // Implement own toString() method
        System.out.println("Test własnej metody toString()");
        System.out.println(point1.toString());
        System.out.println(point2.toString());
        System.out.println(point3.toString());
        System.out.println(point4.toString());
        System.out.println();

        // Implement method to calculate distance between 2 points
        System.out.println("Test metody calcDist()");
        point1.calcDist();
        System.out.println();
        point2.calcDist();
        System.out.println();
        point3.calcDist();
        System.out.println();
        point4.calcDist();
        System.out.println();












    }
}
