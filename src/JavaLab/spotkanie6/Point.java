package JavaLab.spotkanie6;

/**
 * Created by bartlomiej.moryc on 17.01.2017.
 */

import java.util.Scanner;

// Zaimplementuj klasę Point będącą obiektową reprezentacją punktu na płaszczyznie.

public class Point {

    // klasa ma dwa pola x i y typu double
    double x;
    double y;

    // klasa ma konstruktor bezargumentowy tworzący punkt [0,0]
    public Point() {
        x = 0;
        y = 0;
    }

    // klasa ma konstruktor przyjmujący współrzędne [x,y]
    public Point(double a, double b) {
        x = a;
        y = b;
    }

    // zaimplementuj własną wersję metody toString(), aby punkt był wypisywany jako np. [10,-1]
    public String toString() {
        return "[" + (int)this.x + "," + (int)this.y + "]";
    }


    // klasa ma metodę obliczającą odległość pomiedzy dwoma punktami
    public double calcDist(){

        System.out.println("Obliczas dystans pomiedzy punktem A o współrzędnych " + this.toString() + " a punktem B.");

        Scanner input = new Scanner(System.in);

        System.out.print("Podaj wartość X dla punktu B:");
        double xx = input.nextDouble();

        System.out.print("Podaj wartość Y dla punktu B:");
        double yy = input.nextDouble();

        Point pointB = new Point(xx,yy);
        Point pointC = new Point(this.x, pointB.y);

        //pointB.toString();
        //pointC.toString();

        double aLength = Math.abs(pointC.x - pointB.x);
        double bLength = Math.abs(pointC.y - this.y);

        //System.out.println(aLength);
        //System.out.println(bLength);

        System.out.println("Dystans miedzy punktami " + this.toString() + " i " + pointB.toString() + " wynosi: " + Math.sqrt(Math.pow(aLength, 2) + Math.pow(bLength, 2)));
        return Math.sqrt(Math.pow(aLength, 2) + Math.pow(bLength, 2));

    }

}
