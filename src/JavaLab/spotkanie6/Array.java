package JavaLab.spotkanie6;

/**
 * Created by bartlomiej.moryc on 14.01.2017.
 */


import java.util.Arrays;
import java.util.Scanner;



public class Array {


    // metoda wypisująca array

    public static void printTab(int[] t){
        System.out.println("Wypisz elementy tablicy: " + Arrays.toString(t));
    }


    // metoda wypisująca parzyste lub nieparzyste

    public static void parzysTab(int[] t, boolean odd ) {

        if (odd == true) {

            System.out.print("Wypisz nieparzyste: ");

            for (int i = 0; i < t.length; ++i) {

                if (t[i] % 2 != 0) System.out.print(t[i] + " ");
            }

        } else {

            System.out.print("Wypisz parzyste: ");

            for (int i = 0; i < t.length; ++i) {

                if (t[i] % 2 == 0) System.out.print(t[i] + " ");
            }
        }
        System.out.println("");
    }


    // metoda wyznaczająca maks i min z tablicy
    public static void minMaks(int[] t){

        // stwórz kopię array
        int[] dest = new int[t.length];
        System.arraycopy( t, 0, dest, 0, t.length );

        // wyznacz min i maks
        java.util.Arrays.sort(dest);
        System.out.println("Wyznacz minimum: " + dest[0] + " i maksimum: " + dest[dest.length-1]);
    }


    // metoda wyznaczająca medianę

    public static void medianaTab(int[] t) {

        // stwórz kopię array
        int[] dest = new int[t.length];
        System.arraycopy( t, 0, dest, 0, t.length );

        //wyznacz medianę
        java.util.Arrays.sort(dest);
        System.out.println("Wyznacz medianę: " + dest[(int)Math.ceil(dest.length / 2)]);
    }


    // metoda wykorzystująca binary search

    public static void searchTab(int[] t) {

        // stwórz kopię array
        int[] dest = new int[t.length];
        System.arraycopy( t, 0, dest, 0, t.length );

        //szukaj zadanego inta
        System.out.print("Jakiego inta szukasz?");

        Scanner input = new Scanner(System.in);
        int searchVal = input.nextInt();

        java.util.Arrays.sort(dest);
        System.out.println("Posortowana tebala: " + Arrays.toString(dest));
        int returnVal = java.util.Arrays.binarySearch(dest,searchVal);

        if (returnVal >= 0) {
            System.out.println("Poszukiwana wartość posiada index: " + returnVal);
        } else {
            System.out.println("W tabeli nie ma poszukiwanej wartości");
        }
    }












    // metoda main

    public static void main(String[] args) {

        int[] tab1 = new int[] {12,67,17,5,9};

        printTab(tab1);
        parzysTab(tab1, true);
        minMaks(tab1);
        medianaTab(tab1);
        searchTab(tab1);
    }



}
