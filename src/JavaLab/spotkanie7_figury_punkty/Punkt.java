package JavaLab.spotkanie7_figury_punkty;

/**
 * Created by bartlomiej.moryc on 05.02.2017.
 */
public class Punkt {

    private double x;
    private double y;
    private double z;

    //konstruktor bezargumentowy
    public Punkt() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    //konstruktor ze współrzędnymi na płąszczyznie (2D)
    public Punkt(double x, double y) {
        this.x = x;
        this.y = y;
        this.z = 0;
    }

    //konstruktor ze współrzędnymi w przestrzeni (3D)
    public Punkt(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    //toString
    @Override
    public String toString() {
        return "[" + this.x + ", " + this.y + ", " + this.z + "]";
    }

    //oblicz dystans miedzy dwoma punktami
    public double calcDist(Punkt pktB){

        Punkt pktC = new Punkt(this.x, pktB.y);

        double aLength = Math.abs(pktC.x - pktB.x);
        double bLength = Math.abs(pktC.y - this.y);

        return Math.sqrt(Math.pow(aLength, 2) + Math.pow(bLength, 2));

    }

}
