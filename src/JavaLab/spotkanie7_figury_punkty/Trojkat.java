package JavaLab.spotkanie7_figury_punkty;

/**
 * Created by bartlomiej.moryc on 28.01.2017.
 */
public abstract class Trojkat extends Figura{

    private double sideAB, sideBC,sideCA;

    public Trojkat(Punkt pktA, Punkt pktB, Punkt pktC) {
        this.sideAB = pktA.calcDist(pktB);
        this.sideBC = pktB.calcDist(pktC);
        this.sideCA = pktC.calcDist(pktA);
    }

    @Override
    public double obwod() {
        return this.sideAB + this.sideBC + this.sideCA;
    }

    @Override
    public String toString() {
        return "Trojkat [" + this.sideAB + "," + this.sideBC + "," + this.sideCA + "]";
    }
}
