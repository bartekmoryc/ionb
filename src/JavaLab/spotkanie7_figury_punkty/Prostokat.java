package JavaLab.spotkanie7_figury_punkty;

/**
 * Created by bartlomiej.moryc on 28.01.2017.
 */
public class Prostokat extends Figura{

    protected double sideA, sideB;

    public Prostokat(double a, double b){
        this.sideA = a;
        this.sideB = b;
    }
    @Override
    public double obwod() {
        return (2 * this.sideA) + (2 * this.sideB);
    }

    @Override
    public double pole() {
        return this.sideA * this.sideB;
    }

    public double przekatna() {return Math.sqrt(Math.pow(this.sideA, 2) + Math.pow(this.sideB, 2)); }

    @Override
    public String toString() {
        return "Prostokat [" + this.sideA + "," + this.sideB + "]";
    }


}
