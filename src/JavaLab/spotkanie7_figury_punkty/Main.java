package JavaLab.spotkanie7_figury_punkty;


/**
 * Created by bartlomiej.moryc on 28.01.2017.
 */
public class Main {

    public static void main(String[] args) {

        //Punkt
        Punkt pkt1 = new Punkt();
        System.out.println("pkt1 " + pkt1);

        Punkt pkt2 = new Punkt(2, 5);
        System.out.println("pkt2 " + pkt2);

        Punkt pkt3 = new Punkt(17, Math.sqrt(2), -256);
        System.out.println("pkt3 " + pkt3);

        System.out.println("dystans od " + pkt1 + " do " + pkt2 + " wynosi " + pkt1.calcDist(pkt2));




//
//        // Test Trojkat
//
//        Trojkat t1 = new TrojkatRownoboczny(15);
//        System.out.println(t1);
//        System.out.println(t1.obwod());
//
//
//        // Test Prostokat
//
//        Prostokat p1 = new Prostokat(2,5);
//        System.out.println(p1);
//        System.out.println(p1.obwod());
//        System.out.println(p1.pole());









    }





}
