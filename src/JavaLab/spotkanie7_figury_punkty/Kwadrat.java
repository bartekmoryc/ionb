package JavaLab.spotkanie7_figury_punkty;

/**
 * Created by bartlomiej.moryc on 28.01.2017.
 */
public class Kwadrat extends Prostokat {

    public Kwadrat(double a) {
        super(a,a);
    }

    @Override
    public double obwod(){
        return 4 * sideA;
    }

    @Override
    public double pole() {
        return Math.pow(sideA, 2);
    }
}
