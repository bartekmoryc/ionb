package JavaLab.spotkanie4.s4z7;

/**
 * Created by bartlomiej.moryc on 20.11.2016.
 */
public class MultiplicationTable {

    /** Main method */
    public static void main(String[] args) {
        // Display the table heading
        System.out.println("           Multiplication Table");

        // Display the number title
        System.out.print("    ");
        for (int j = 1; j <= 10; j++)
            System.out.print("   " + j);

        System.out.println("\n-----------------------------------------");

        // Print table body
        for (int i = 1; i <= 10; i++) {
            System.out.print(i + " | ");
            for (int j = 1; j <= 10; j++) {
                // Display the product and align properly
                System.out.printf("%4d", i * j);
            }
            System.out.println();
        }
    }
}
