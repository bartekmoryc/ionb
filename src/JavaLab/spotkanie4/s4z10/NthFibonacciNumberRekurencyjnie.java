package JavaLab.spotkanie4.s4z10;

/**
 * Created by bartlomiej.moryc on 20.11.2016.
 */

import java.util.Scanner;

public class NthFibonacciNumberRekurencyjnie {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        // Zapytanie o porzadkową
        System.out.print("Którą z kolei cyfrę z ciagu Fibonacciego wyświetlić? ");
        int number = input.nextInt();

        System.out.print(number + ". cyfra ciagu Fibonacciego to " + fibonacci(number));
    }

    public static int fibonacci(int n) {

        // Kalkulowanie cyfry z ciągu Fibonacciego
        if (n < 2) {
            return n;
        }
        else {
            return fibonacci(n-1) + fibonacci(n-2);
        }
    }
}
