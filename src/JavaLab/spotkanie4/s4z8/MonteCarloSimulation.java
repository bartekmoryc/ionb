package JavaLab.spotkanie4.s4z8;

/**
 * Created by bartlomiej.moryc on 20.11.2016.
 */
public class MonteCarloSimulation {

    public static void main(String[] args) {
        final int NUMBER_OF_TRIALS = 10000000;
        int numberOfHits = 0;

        for (int i = 0; i < NUMBER_OF_TRIALS; i++) {
            double x = Math.random() * 2.0 - 1;
            double y = Math.random() * 2.0  - 1;
            if (x * x + y * y <= 1)
                numberOfHits++;
        }

        double pi = 4.0 * numberOfHits / NUMBER_OF_TRIALS;
        System.out.println("PI is " + pi);
    }
}
