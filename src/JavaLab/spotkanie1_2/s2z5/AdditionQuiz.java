package JavaLab.spotkanie1_2.s2z5;

/**
 * Created by bartlomiej.moryc on 23.10.2016.
 */

import java.util.Scanner;

public class AdditionQuiz {
    public static void main(String[] args) {
        byte number1 = (byte)(System.currentTimeMillis() % 10);
        byte number2 = (byte)(System.currentTimeMillis() / 10 % 10);

        // Create a Scanner
        Scanner input = new Scanner(System.in);

        System.out.print(
                "What is " + number1 + " + " + number2 + "? ");

        int answer = input.nextInt();

        System.out.println(
                number1 + " + " + number2 + " = " + answer + " is " +
                        (number1 + number2 == answer));
    }
}
