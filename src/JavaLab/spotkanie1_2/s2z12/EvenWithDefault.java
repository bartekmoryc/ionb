package JavaLab.spotkanie1_2.s2z12;

/**
 * Created by bartlomiej.moryc on 23.10.2016.
 */

import java.util.Scanner;

public class EvenWithDefault {
    public static void main(String[] args) {
        //tworzenie Scannera
        Scanner input = new Scanner(System.in);

        //prośba o input
        System.out.print("Podaj liczbę: ");
        int digit = input.nextInt();

        switch (digit % 2) {
            case 0: System.out.println("Parzysta"); break;
            default: System.out.println("Nieparzysta");
        }

    }
}
