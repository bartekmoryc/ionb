package JavaLab.spotkanie1_2.s2z2;

/**
 * Created by bartlomiej.moryc on 23.10.2016.
 */

import java.util.Scanner;

public class ComputeArea {
    public static void main(String[] args) {
        double radius; // Declare radius
        double area; // Declare area


        // Assign a radius
        System.out.print("Define radius: ");
        Scanner input = new Scanner(System.in);
        radius = input.nextDouble();

        // Compute area
        area = radius * radius * 3.14159;

        // Display results
        System.out.println("The area for the circle of radius " +
                radius + " is " + area);
    }
}