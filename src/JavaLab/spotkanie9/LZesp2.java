package JavaLab.spotkanie9;

/**
 * Created by bartlomiej.moryc on 11.02.2017.
 */
public class LZesp2 extends LZesp {

    private double re;
    private double im;

    // konstr kopiujący
    public LZesp2(LZesp l) {
        this.re = l.getRe();
        this.im = l.getIm();
    }

    public LZesp2() {
        super();
    }

    public LZesp2(double are, double aim) {
        super(are, aim);
    }

    @Override
    public String jakoLancuch() {

        if ((re == 0) && (im == 0)){
            return "0";
        } else if (re == 0 && im < 0) {
            return im + "i";
        } else if (re == 0) {
            return im + "i";
        } else if (im == 0) {
            return re +"";
        } else if (im < 0) {
            return re + " - " + im + "i";
        } else {
            return "!!!";
        }
    }







}
