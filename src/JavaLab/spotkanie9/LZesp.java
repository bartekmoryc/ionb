package JavaLab.spotkanie9;

/**
 * Created by bartlomiej.moryc on 11.02.2017.
 */
public class LZesp {
    private double re;
    private double im;

    public LZesp() {
        re = 0.0;
        im = 0.0;
    }
    public LZesp(double are, double aim) {
        re = are;
        im = aim;
    }

    public double getRe() {
        return re;
    }

    public double getIm() {
        return im;
    }

    public void pisz() {
        System.out.print(jakoLancuch());
    }

    public String jakoLancuch() {
        return new String(re+" + "+im+"i");
    }

    public LZesp plus(LZesp l) {
        return new LZesp(re + l.getRe(), im + l.getIm());
    }

    public LZesp minus(LZesp l) {
        return new LZesp(re - l.getRe(), im - l.getIm());
    }

    public LZesp razy(LZesp l) {
        return new LZesp(
                re * l.getRe() - im * l.getIm(),
                re * l.getIm() + im * l.getRe()
        );
    }
}
