package JavaLab.spotkanie9;

/**
 * Created by bartlomiej.moryc on 11.02.2017.
 */
public class Main {

    public static void main(String args[]) {
//        LZesp l1 = new LZesp(3,2);
//        LZesp l2 = new LZesp(2,3);
//
//        System.out.println(l1.jakoLancuch() + " + " +
//                l2.jakoLancuch() + " = " +
//                l1.plus(l2).jakoLancuch()
//        );
//        System.out.println(l1.jakoLancuch() + " - " +
//                l2.jakoLancuch() + " = " +
//                l1.minus(l2).jakoLancuch()
//        );
//        System.out.println(l1.jakoLancuch() + " * " +
//                l2.jakoLancuch() + " = " +
//                l1.razy(l2).jakoLancuch()
//        );



        LZesp l1 = new LZesp(3,2);
        LZesp l2 = new LZesp(2,3);

        System.out.println(l1.jakoLancuch() + " + " +
                l2.jakoLancuch() + " = " +
                l1.plus(l2).jakoLancuch()
        );
        System.out.println(l1.jakoLancuch() + " - " +
                l2.jakoLancuch() + " = " +
                new LZesp2(l1.minus(l2)).jakoLancuch()
        );
        System.out.println(l1.jakoLancuch() + " * " +
                l2.jakoLancuch() + " = " +
                l1.razy(l2).jakoLancuch()
        );
    }
}
