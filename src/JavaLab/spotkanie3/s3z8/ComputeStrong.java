package JavaLab.spotkanie3.s3z8;

/**
 * Created by bartlomiej.moryc on 05.11.2016.
 */
import java.util.Scanner;

public class ComputeStrong {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // Zapytanie o liczbę całkowitą
        System.out.print("Podaj liczbę całkowitą: ");
        long number = Math.abs(input.nextLong());

        // Silnia dla number == 0
        if (number == 0) {
            System.out.println("Silnia z " + number + " to 1");
        }

        // Oblicz silnię dla number != 0
        else {
            long result = number;

            for (long i = number-1; i > 0; i--) {
                result = result * i;
            }
            // Wyświetl wynik
            System.out.println("Silnia z " + number + " to " + result);
        }
    }
}


