package JavaLab.spotkanie3.s3z9;

/**
 * Created by bartlomiej.moryc on 05.11.2016.
 */
import java.util.Scanner;

public class NthFibonacciNumberIteracyjnie {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        // Zapytanie o porzadkową
        System.out.print("Którą z kolei cyfrę z ciagu Fibonacciego wyświetlić? ");
        int ordinal = input.nextInt();



        // Kalkulowanie cyfry z ciągu Fibonacciego
        int fibonacciResult = 0;
        int fibonacci1 = 0;
        int fibonacci2 = 1;

        for (int i = 1; i < ordinal; i++) {
            fibonacciResult = fibonacci1 + fibonacci2;
            //System.out.print(fibonacciResult);
            fibonacci1 = fibonacci2;
            //System.out.print(fibonacci1);
            fibonacci2 = fibonacciResult;
            //System.out.print(fibonacci2);
        }
        System.out.print("Ta cyfra to: " + fibonacciResult);
    }
}
