package JavaLab.spotkanie3.s3z11;

/**
 * Created by bartlomiej.moryc on 11.11.2016.
 */

import java.util.Scanner;

public class DecimalToBinary {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        // Zapytanie o liczbę dzisętną
        System.out.print("Podaj liczbę w systemie dziesiętnym: ");
        double decimal = input.nextInt();

        // Przeliczenie decimal na binary


        int count = String.valueOf(decimal).length() * 3; // deklaracja zmiennej iteracyjnej
        System.out.println("count: " + count);


        String binary = "";

        for (int i = count; i >= 0; i--) {

            Double resultInDouble = Math.floor(decimal / Math.pow(2,i)); //
            //System.out.println("resultInDouble: " + resultInDouble);

            Integer resultInInteger = resultInDouble.intValue();
            //System.out.println("resultInInteger: " + resultInInteger);

            binary += Integer.toString(resultInInteger);
            //System.out.println("binary: " + binary);

            decimal = decimal % Math.pow(2,i);
            //System.out.println("decimal: " + decimal);
        }

        System.out.println("Binarnie: " + binary);

    }
}
