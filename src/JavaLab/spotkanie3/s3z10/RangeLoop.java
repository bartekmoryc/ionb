package JavaLab.spotkanie3.s3z10;

/**
 * Created by bartlomiej.moryc on 06.11.2016.
 */

import java.util.Scanner;

public class RangeLoop {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        // Pętla weryfikacyjna
        int number = 0;
        do {
            System.out.print("Podaj liczbę całkowitą spoza przedziału 0 - 15 włącznie: ");
            number = input.nextInt();
        }

        while (number < 0 || number > 15);
    }
}
