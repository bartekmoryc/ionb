package JavaLab.spotkanie5;

/**
 * Created by bartlomiej.moryc on 04.12.2016.
 */
import java.util.Scanner;

public class TablicaJednowymiarowa {


    // generowanie zawartosci tabeli
    static int[] generujTabele () {

        Scanner input = new Scanner(System.in);

        //definiowanie rozmiaru tablicy
        System.out.println("Podaj rozmiar tablicy: ");

        int tabSize = input.nextInt();

        int[] tab = new int[tabSize];


        System.out.println("Rozmiar tablicy: " + tab.length);

        // Losowanie zawartość
        for (int i = 0; i < tab.length; i++) {
            tab[i] = (int) (Math.random() * 100);
        }

        return tab;
    }

    // wypisywanie zawartosci tabeli
    static void wypiszZawartoscTabeli (int[] A) {

        // wypisywanie zawartości
        for (int i = 0; i < A.length; i++) {
            System.out.println("Pozycja " + (i + 1) + " : " + Integer.toString(A[i]));
        }
    }

    // znajdowanie najwiekszej sposrod wartosci w tabeli
    static void wypiszMax (int[] A) {

        //Wartość maxymalna
        int max = 0;
        for (int i = 0; i < A.length; i++) {
            if (A[i] > max) {
                max = A[i];
            }
        }
        System.out.println("Max: " + max);
    }

        //Wartość minimalna



/*
        //Średnia
        float sum = 0;
        for (int i = 0; i < A.length; i++) {
            sum += A[i];
        }
        System.out.println("Suma: " + sum);
        System.out.println("Średnia: " + (sum / (A.length)));

    }
*/
    public static void main(String[] args) {

        int[] tabela = generujTabele();

        wypiszZawartoscTabeli(tabela);

        wypiszMax(tabela);


    }
}
